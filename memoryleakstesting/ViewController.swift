//
//  ViewController.swift
//  memoryleakstesting
//
//  Created by Alexander Khodko on 13/01/2017.
//  Copyright © 2017 Alexander Khodko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var id = "582d63e8b7d1f9001ef10979"
    var restaurantVC: [Restaraunt]?
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 200))
        button.backgroundColor = .red
        button.addTarget(self, action: #selector(request), for: .touchUpInside)
        self.view.addSubview(button)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.request()
    }
    
    func request() {
        //id - с пердыдущих экранов
        FullRestaurantTableModule.requestRestaurant(handler: { [weak self] restaurants in
            guard let `self` = self else {
                return
            }
            self.restaurantVC = restaurants
        })
    }
}

