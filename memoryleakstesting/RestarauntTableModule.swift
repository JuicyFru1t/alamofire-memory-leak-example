//
//  RestarauntTableModule.swift
//  memoryleakstesting
//
//  Created by Alexander Khodko on 13/01/2017.
//  Copyright © 2017 Alexander Khodko. All rights reserved.
//

import Foundation
import SwiftyJSON

class FullRestaurantTableModule {
    class func requestRestaurant(handler: @escaping ([Restaraunt]) -> Void) {
        APIModel.sharedInstance.request(.post, URL: "restaurants/near", parameters: nil, completionHandler: { (data, _) in
            guard let `data` = data else {
                return
            }
            print(data)
            let object = data["restaurants"]
            if let dataObj = object.to(type: Restaraunt.self) {
                handler(dataObj as! [Restaraunt])
            }
        })
    }
}
