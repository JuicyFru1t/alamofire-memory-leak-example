//
//  API.swift
//  memoryleakstesting
//
//  Created by Alexander Khodko on 13/01/2017.
//  Copyright © 2017 Alexander Khodko. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class APIModel {
    private init() {}
    static let sharedInstance = APIModel()
    
    //тут несколько сервер для удобства, по идее не влияет
    enum ServerAddress: String {
        case developer = "http://dev.bitkom.su:8080/v2"
    }
    private let serverAddress = ServerAddress.developer.rawValue
    public typealias CompletionHandler = (JSON?, NSError?) -> Void
    
    public func request(_ httpmethod: Alamofire.HTTPMethod, URL: String, parameters: [String: AnyObject]?, completionHandler: @escaping CompletionHandler) -> Void {
        
        var header: HTTPHeaders = [:]
        
        
        //хедер из user defaults обычно
        header["Authorization"] = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjU4NzhkZTk2NWY1NDUzMDAxNjAwNjM1NiIsImlhdCI6MTQ4NDMxNjMxMCwiZXhwIjoxNTE1ODUyMzEwfQ.cRLCJXogSS63fCt6WHDXSME3IWjeGnGgZ99VzsiPA8A"
        
        Alamofire.request("\(serverAddress)/\(URL)", method: httpmethod, parameters: parameters, encoding: JSONEncoding.default, headers: header).validate().responseJSON { response in
            let error: NSError? = response.result.error as NSError?
            if response.result.value != nil {
                let data = JSON(response.result.value!)
                completionHandler(data, error)
            } else {
                completionHandler(nil, error)
            }
        }
    }
}
