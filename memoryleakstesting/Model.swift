//
//  Data.swift
//  memoryleakstesting
//
//  Created by Alexander Khodko on 13/01/2017.
//  Copyright © 2017 Alexander Khodko. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

//протокол и расширение взяты с какого то тикета на gite swiftyjson
protocol JSONable {
    init?(data: JSON)
}

extension JSON {
    func to<T>(type: T?) -> Any? {
        if let baseObj = type as? JSONable.Type {
            if self.type == .array {
                var arrObject: [Any] = []
                for obj in self.arrayValue {
                    let object = baseObj.init(data: obj)
                    arrObject.append(object!)
                }
                return arrObject
            } else {
                let object = baseObj.init(data: self)
                return object!
            }
        }
        return nil
    }
}

//тоже менял все атрибуты на опционалы и так далее, не помогало
class Restaraunt: JSONable {
    let id: String
    
    let name: String
    let image: String
    let description: String
    let mapType: String
    let phone: String
    let workTime: String
    let avarageBill: String
    let rating: String
    let ratingTrend: String
    
    let specialOfferName: String?
    let specialOfferDescription: String?
    
    
    var address: Address = Address()
    var schedule: [ReservationSchedule] = []
    var avaliableTimes: [Schedule] = []
    let croud: CroudInfo
    var alert: Alert? = nil
    
    var isFavourited: Bool
    let isActive: Bool
    let isWeeklyTop: Bool?
    let isMonthlyTop: Bool?
    let isYearTop: Bool?
    var doesHaveEvent: Bool
    var doesHaveDiscount: Bool
    
    ///Функция, возвращающая Restaurant из JSON
    required init(data: JSON) {
        self.id                       = data["id"].stringValue
        self.name                     = data["name"].stringValue
        self.image                    = data["img"].stringValue
        self.description              = data["shortDesc"].stringValue
        self.phone                    = data["phone"].stringValue
        self.workTime                 = data["currentWork"].stringValue
        self.avarageBill              = data["price"].stringValue
        self.rating                   = data["rating"].stringValue
        self.ratingTrend              = data["trend"].stringValue
        
        self.mapType                  = data["mapType"].stringValue
        self.specialOfferDescription  = data["discounts"]["name"].string
        self.specialOfferName         = data["discounts"]["type"].string
        
        if let alertData = data["label"].to(type: Alert.self) {
            self.alert = alertData as? Alert
        }
        
        if let avaliableTimesData = data["scheduleRes"].to(type: Schedule.self) {
            self.avaliableTimes = avaliableTimesData as? [Schedule] ?? []
        }
        
        if let scheduleData = data["availableDates"].to(type: ReservationSchedule.self) {
            self.schedule = scheduleData as? [ReservationSchedule] ?? []
        }
        
        self.address.metrostationName   = data["address"]["metro"]["name"].stringValue
        self.address.latitude           = data["address"]["location"][1].doubleValue
        self.address.longitude          = data["address"]["location"][0].doubleValue
        self.address.address            = data["address"]["street"].stringValue
        self.address.mapType            = data["mapType"].stringValue
        self.address.distance           = data["distance"].doubleValue
        
        self.croud                    = CroudInfo(rawValue: data["croud"].stringValue) ?? .avarage
        self.isFavourited             = data["isFavourited"].boolValue
        self.doesHaveDiscount         = data["discounts"]["id"].string != nil
        self.doesHaveEvent            = data["events"]["id"].string != nil
        self.isWeeklyTop              = data["weeklyTop"].bool
        self.isMonthlyTop             = data["monthlyTop"].bool
        self.isYearTop                = data["yearTop"].bool
        self.isActive                 = data["isActive"].boolValue
    }
}

struct Address {
    var metrostationName: String! = nil
    var latitude: Double! = nil
    var longitude: Double! = nil
    var address: String! = nil
    var mapType: String! = nil
    var distance: Double! = nil
}

class Schedule: JSONable {
    var time: String
    required init(data: JSON) {
        self.time = String(describing: data["time"].object)
    }
}

class Tag: JSONable {
    
    var name: String
    var image: String
    
    required init(data: JSON) {
        self.name = data["name"].stringValue
        self.image = data["img"].stringValue
    }
}

enum CroudInfo: String {
    case bitkom = "Битком"
    case free = "Свободно"
    case avarage = "Загруженно"
    
    func values() -> (title: String, image: UIImage, textColor: UIColor) {
        switch self {
        case .bitkom:
            return (self.rawValue, UIImage(named: "redValue")!, UIColor(red: 208/255, green: 19/255, blue: 0/255, alpha: 1))
        case .free:
            return (self.rawValue, UIImage(named: "greenValue")!, UIColor(red: 84/255, green: 175/255, blue: 98/255, alpha: 1))
        case .avarage:
            return(self.rawValue, UIImage(named: "yellowValue")!, UIColor(red: 247/255, green: 157/255, blue: 0/255, alpha: 1))
        }
    }
}

class ReservationSchedule: JSONable {
    
    var description: String
    var data: String
    var dayTitle: String
    var fullTitle: String
    
    required init(data: JSON) {
        self.data = data["value"].stringValue
        self.dayTitle = data["rDay"].stringValue
        self.description = data["label"].stringValue
        self.fullTitle = data["rTitle"].stringValue
    }
}

class Alert: JSONable {
    
    let text: String
    let icon: String
    let backgroundImage: String
    
    required init(data: JSON) {
        self.text = data["name"].stringValue
        self.icon = data["type"].stringValue
        self.backgroundImage = data["color"].stringValue
    }
}

